= Home Decorator

This is my script for "decorating" a new home directory on a remote system.

.Decorations Include
. Copy SSH ID
. Setup tmux to autolaunch on SSH connection

Most of the heavy lifting is offloaded to Ansible.
